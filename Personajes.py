# PERSONAJES | Plants vs Zombies 4.0
from Personaje_sprite import *

# OBJETO: Personaje
class Personaje:

	# ATRIBUTOS
	# --------------------
	
	# INFORMACION
	jugador = None
	puntos_vida = 0
	puntos_ataque = 0
	rango_ataque = 0
	rango_mov = 0
	precio = 0
	codigo = None
	sprite = None
	# Interfaz gráfica
	
	# URL IMAGENES
	imagen_Z1 = "MULTIMEDIA/ZOMBIES/nzombie1.png"
	imagen_ZA1 = "MULTIMEDIA/ZOMBIES/bucketheadzombie1.png"
	imagen_PS1 = "MULTIMEDIA/PLANTAS/PeaShooter1.png"
	imagen_N1 = "MULTIMEDIA/PLANTAS/Wallnut_body1.png"
	
	imagen_Z2 = "MULTIMEDIA/ZOMBIES/nzombie2.png"
	imagen_ZA2 = "MULTIMEDIA/ZOMBIES/bucketheadzombie2.png"
	imagen_PS2 = "MULTIMEDIA/PLANTAS/PeaShooter2.png"
	imagen_N2 = "MULTIMEDIA/PLANTAS/Wallnut_body2.png"
	
	imagen_url = None

	# CONSTRUCTOR
	def __init__(self, codigo,jugador):

		# Segmento de código que crea una instancia de personaje de 
		# acuerdo al código recibido.
		
		#--------------------------------------------------
		#						SERVIDOR
		
		if codigo == "Z001":

			self.puntos_vida = 100
			self.puntos_ataque = 40
			self.rango_ataque = 1
			self.rango_mov = 1
			self.precio = 25
			self.imagen_url = self.imagen_Z1

		elif codigo == "ZA01":

			self.puntos_vida = 120
			self.puntos_ataque = 40
			self.rango_ataque = 1
			self.rango_mov = 1
			self.precio = 55
			self.imagen_url = self.imagen_ZA1

		elif codigo == "PS01":

			self.puntos_vida = 100
			self.puntos_ataque = 50
			self.rango_ataque = 3
			self.precio = 25
			self.imagen_url = self.imagen_PS1

		elif codigo == "N001":

			self.puntos_vida = 100
			self.puntos_ataque = 0
			self.rango_ataque = 0
			self.precio = 55
			self.imagen_url = self.imagen_N1

		#-----------------------------------------
		#	CLIENTE

		if codigo == "Z002":

			self.puntos_vida = 100
			self.puntos_ataque = 40
			self.rango_ataque = 1
			self.rango_mov = 1
			self.precio = 25
			self.imagen_url = self.imagen_Z2

		elif codigo == "ZA02":

			self.puntos_vida = 120
			self.puntos_ataque = 40
			self.rango_ataque = 1
			self.rango_mov = 1
			self.precio = 55
			self.imagen_url = self.imagen_ZA2

		elif codigo == "PS02":

			self.puntos_vida = 100
			self.puntos_ataque = 50
			self.rango_ataque = 3
			self.precio = 25
			self.imagen_url = self.imagen_PS2

		elif codigo == "N002":

			self.puntos_vida = 100
			self.puntos_ataque = 0
			self.rango_ataque = 0
			self.precio = 55
			self.imagen_url = self.imagen_N2

		# Características generales
		
		self.jugador = jugador
		self.codigo = codigo
		
		# Crea el sprite que se va a dibujar en pantalla
		self.sprite = PersonajeSprite(self.imagen_url)
		print("codigo: %s | precio: %s | img: %s"%(self.codigo,self.precio,self.imagen_url))
