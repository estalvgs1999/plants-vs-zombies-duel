# CODIGO VENTANA MENU GUI | PvZ DUEL 4.0
# -------------------------------------------

# Importar módulos

import pygame
from Boton import *
from Colores import *

# Inicializar
pygame.mixer.init(44100,16,2,4096)

# Crear un objeto: MENU

class Ventana_menu:

	# Atributos
	# -----------------------------
	
	# Variables U.I 
	surface = None
	ancho = 700
	alto = 500

	# Botones
	btn_servidor = None 
	btn_cliente = None
	btn_cerrar = None

	# Imágenes
	img_fondo = None

	img_btn_servidor1 = None
	img_btn_servidor2 = None
	
	img_btn_cliente1 = None
	img_btn_cliente2 = None

	img_btn_cerrar1 = None
	img_btn_cerrar2 = None

	# Métodos
	# -----------------------------
	
	#--------------------------------------------------------------------------------------------
	# FUNCION START | Inicia una nueva ventana
	
	def start(self):

		# Crea la ventana
		pygame.display.set_caption("Plants vs Zombies DUEL")
		self.surface = pygame.display.set_mode((self.ancho,self.alto))

		# Reproduce el Jingle
		
		self.musica()

		# Cargar las imágenes
		
		# FONDO
		self.img_fondo = pygame.image.load("MULTIMEDIA/ENTORNO/MENU.png").convert_alpha()
		self.img_fondo = pygame.transform.scale(self.img_fondo, (700,500))
		self.img_fondo.get_rect().centerx = 700/2
		self.img_fondo.get_rect().centery = 500/2


		# BOTON CLIENTE
		self.img_btn_cliente1 = pygame.image.load("MULTIMEDIA/ENTORNO/client.jpg").convert_alpha()
		self.img_btn_cliente1 = pygame.transform.scale(self.img_btn_cliente1, (181,30))
		self.img_btn_cliente2 = pygame.image.load("MULTIMEDIA/ENTORNO/client2.jpg").convert_alpha()
		self.img_btn_cliente2 = pygame.transform.scale(self.img_btn_cliente2, (181,30))

		# BOTON SERVIDOR
		self.img_btn_servidor1 = pygame.image.load("MULTIMEDIA/ENTORNO/server.jpg").convert_alpha()
		self.img_btn_servidor1 = pygame.transform.scale(self.img_btn_servidor1, (134,30))
		self.img_btn_servidor2 = pygame.image.load("MULTIMEDIA/ENTORNO/server2.jpg").convert_alpha()
		self.img_btn_servidor2 = pygame.transform.scale(self.img_btn_servidor2, (134,30))

		# BOTON CERRAR
		self.img_btn_cerrar1 = pygame.image.load("MULTIMEDIA/ENTORNO/close.jpg").convert_alpha()
		self.img_btn_cerrar1 = pygame.transform.scale(self.img_btn_cerrar1, (76,22))
		self.img_btn_cerrar2 = pygame.image.load("MULTIMEDIA/ENTORNO/close2.jpg").convert_alpha()
		self.img_btn_cerrar2 = pygame.transform.scale(self.img_btn_cerrar2, (76,22))

		# BOTONES
		
		self.btn_servidor = Boton( pos_x= 116, pos_y = 290, alto= 30, ancho =134,img1 = self.img_btn_servidor1,
									img2= self.img_btn_servidor2)
		self.btn_cliente = Boton( pos_x= 428, pos_y = 290, alto= 31, ancho =181,img1 = self.img_btn_cliente1,
									img2= self.img_btn_cliente2)
		self.btn_cerrar = Boton( pos_x= 595, pos_y = 475, alto= 22, ancho = 72,img1 = self.img_btn_cerrar1,
									img2= self.img_btn_cerrar2)

	#--------------------------------------------------------------------------------------------
	# FUNCION MUSICA | Se encarga de reproducir la música en loop mientras la ventana
	# se encuentra abierta.
	
	def musica(self):
		
		pygame.mixer.music.load("MULTIMEDIA/Sounds/Plants vs. Zombies (Main Theme).mp3")
		pygame.mixer.music.set_volume(0.5)
		pygame.mixer.music.play(loops=-1, start=0.0)

	#--------------------------------------------------------------------------------------------
	# FUNCION UPDATE_PANTALLA | Mantiene el contenido actualizado en cada ciclo. 
	
	def update_pantalla(self):

		# PRIMERO se dibuja el fondo
		self.surface.blit(self.img_fondo,self.img_fondo.get_rect())

		# DESPUES se dibuja lo demás
		self.btn_servidor.colocar_btn(self.surface)
		self.btn_cliente.colocar_btn(self.surface)
		self.btn_cerrar.colocar_btn(self.surface)

	#--------------------------------------------------------------------------------------------
	# FUNCION MAIN_LOOP | Se encarga de recibir los eventos y realizar trabajos con ellos
	
	def main_loop(self, evento):
		
		# Obtiene los datos de la posición del mouse
		
		mouse_x = pygame.mouse.get_pos()[0]
		mouse_y = pygame.mouse.get_pos()[1]

		# Llama a las funciones de detección
		
		if evento.type == pygame.MOUSEMOTION:

			self.btn_servidor.check_click(mouse_x, mouse_y)
			self.btn_cliente.check_click(mouse_x, mouse_y)
			self.btn_cerrar.check_click(mouse_x, mouse_y)

		# Método que detecta el click sobre la pantalla
		# ---------------------------------------------
		if evento.type == pygame.MOUSEBUTTONDOWN:

			# Si se clickeo sobre un botón, actuará
			if self.btn_servidor.check_click(mouse_x, mouse_y):
				print("Click en el boton Servidor")
				return 1
			if self.btn_cliente.check_click(mouse_x, mouse_y):
				print("Click en el boton Cliente")
				return 2
			if self.btn_cerrar.check_click(mouse_x, mouse_y):
				print("Click en el boton Close")
				return 3
		return 0
		
#--------------------------------------------------------------------------------------------
#								FIN DEL CODIGO

