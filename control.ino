// SKETCH CONTROL | Plants vs Zombies DUEL | ARCADE v 1.0
// ------------------------------------------------------
// Control que tendrá comunicación serial con la computadora para permitir el juego
// de Plants vs Zombies DUEL(PROYECTO II).
// Partes:
// * 5 pulsadores
// * 2 LED
// * Joystick
// * Lib <>

// Declarar las funciones que se utilizarán en el programa

void JuegoLuces();
uint8_t flancoSubida(int btn);
void controlBotones();

// Declarar las variables

const int EJEX = A0;
const int EJEY = A1;

int valorX;
int valorY;

// Botones de selección de personaje

#define BTN_Z  0
#define BTN_ZA 1
#define BTN_PS 2
#define BTN_N  3
#define BTN_LISTO 4

// ACTUADORES
#define LED_S  0
#define LED_C  1

boolean LED_STATE1 = false;
boolean LED_STATE2 = false;

/********************************** ARRAYS ***********************************/

//Pines BTN
uint8_t botones[5] = {2, 3, 4, 5, 6};

/* LOS PULSADORES SE ACTVAN EN FLANCO DE SUBIDA(PULL-UP)
 0 ______		  _____
 1       _________  <---- El arduino lee 1 en el flanco de subida
*/

uint8_t actuador_pin[2] = {9, 8};

//Este array contiene el último estado
uint8_t button_state[5]; 

/*
	int son variables de tipo entero --> +/- (32768)
	uint8_t se utiliza para guardar datos que ocupen un byte(8 bits) o menos
	lo que permite ahorrar memoria.
	(También existen uint16_t y uint32_t, para 32 bits y 16 bits)
 */

// FUNCIONES

// Funcion que se encarga de oscilar los LED en un patrón de patrulla

void JuegoLuces()
{
	// Juego de luces de inicio
	for(int cont = 0; cont < 4; cont++)
	{
		digitalWrite(actuador_pin[LED_S], HIGH);
		delay(200);
		digitalWrite(actuador_pin[LED_S], LOW);

		digitalWrite(actuador_pin[LED_C], HIGH);
		delay(200);
		digitalWrite(actuador_pin[LED_C], LOW);
	}
}

// Función de lectura y comparación de pulsadores
// se encarga de recibir el botón ingresado, y lo coloca como
// un estado actual | Esto se hace para evitar multiples entradas(Por el %error mecánico
// al hacer la pulsación)

uint8_t flancoSubida(int btn)
{
	uint8_t valor_nuevo = digitalRead(botones[btn]);
	uint8_t result = button_state[btn] != valor_nuevo && valor_nuevo == 1;
	button_state[btn] = valor_nuevo;
	return result;
}

void controlBotones()
{	
	/*
	-------------------------------------------------------------------------------
	EL CODIGO DEBIA SER SENCILLO PARA FACILITAR LA LECTURA DE LOS BYTES ENVIADOS
	A PYTHON, PARA SU INTERPRETACIÓN EN EL JUEGO. POR LO TANTO SE DA UN DESGLOSE DE 
	LOS CÓDIGOS DE LOS PERSONAJES:

	+ ZOMBIE | 0 (b'0')
	+ ZOMBIE ARMADO | 1 (b'1')
	+ PEASHOOTER | 2 (b'2')
	+ NUEZ | 3 (b'3')
	+ LISTO | 4 (b'4')
	
	-------------------------------------------------------------------------------
	 */
	 
	// Control de flujo de el control de los pulsadores | Busca si algún botón fue presionado
	
	if(flancoSubida(BTN_Z))
	{
		Serial.println("0");
	}

	if(flancoSubida(BTN_ZA))
	{
		Serial.println("1");
	}

	if(flancoSubida(BTN_PS))
	{
		Serial.println("2");
	}

	if(flancoSubida(BTN_N))
	{
		Serial.println("3");

	}

	if(flancoSubida(BTN_LISTO))
	{
		Serial.println("4");
	}
}
// -----------------------------------------------------------------------------

//Función setup
void setup()
{
	//inicializarpulsadores
	pinMode(botones[BTN_Z], INPUT_PULLUP);
	pinMode(botones[BTN_ZA], INPUT_PULLUP);
	pinMode(botones[BTN_PS], INPUT_PULLUP);
	pinMode(botones[BTN_N], INPUT_PULLUP);
	pinMode(botones[BTN_LISTO], INPUT_PULLUP);

	//Declarar actuadores
	pinMode(actuador_pin[LED_S], OUTPUT);
	pinMode(actuador_pin[LED_C], OUTPUT);

	//Inicializar button_state
	button_state[0] = 1;
	button_state[1] = 1;
	button_state[2] = 1;
	button_state[3] = 1;
	button_state[4] = 1;

	// Iniciar el puerto Serie | Configura los datos a 9600 bps
	Serial.begin(9600);
	Serial.println(">>> PUERTO SERIE INICIADO");
	JuegoLuces();

}

void loop()
{
	// ESTADOS DE LOS BOTONES

	controlBotones();

	//  LEE DE PYTHON
	if (Serial.available() > 0) // Solo entra si ha llegado un dato
	{
		char listo = Serial.read(); // Lee de Serie
		
		// Ha llegado mensaje del contrario(El oponente está listo)
		if (listo == '9')
		{
			if (LED_STATE2 == true)
			{
				digitalWrite(actuador_pin[LED_C],HIGH);
				delay(1000);
				digitalWrite(actuador_pin[LED_C],LOW);
				digitalWrite(actuador_pin[LED_S],LOW);
				LED_STATE2 = false;
			} else {

				digitalWrite(actuador_pin[LED_C],HIGH);
				LED_STATE1 = true;
			}
		} else if (listo == '8') // Ha llegado mensaje del jugador(El jugador está listo)
		{
			if (LED_STATE1 == true)
			{
				digitalWrite(actuador_pin[LED_S],HIGH);
				delay(1000);
				digitalWrite(actuador_pin[LED_S],LOW);
				digitalWrite(actuador_pin[LED_C],LOW);
				LED_STATE1 = false;
			} else {

				digitalWrite(actuador_pin[LED_S],HIGH);
				LED_STATE2 = true;
			}
		}
	}

	// LECTURA Y ESCRITURA DEL JOYSTICK
	
	valorX = analogRead(EJEX);
	valorY = analogRead(EJEY);

	// Realizar acciones de acuerdo a la posición de los ejes (x,y)
	
	if (valorX == 0)
	{
		Serial.println("D"); // DOWN
		delay(500);
	
	} else if(valorX == 1023)
	{
		Serial.println("U"); // UP
		delay(500);
	} 

	if (valorY == 0)
	{
		Serial.println("L"); // LEFT
		delay(500);
	
	} else if(valorY == 1023)
	{
		Serial.println("R"); // RIGHT
		delay(500);
	} 


}