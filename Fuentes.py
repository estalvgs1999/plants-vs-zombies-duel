# FUENTES DISPONIBLES
#------------------------------

#Importar módulos
import pygame

# Inicializar
pygame.font.init()

# FUENTES
font_gde = pygame.font.SysFont("MonsterFonts-HouseofTerror", 50)
font_med = pygame.font.SysFont("MonsterFonts-HouseofTerror", 35)
font_peq = pygame.font.SysFont("MonsterFonts-HouseofTerror", 20)