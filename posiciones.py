# POSICIONES CORRECTAS PARA LAS IMAGENES | Plants vs Zombies DUEL 4.0


def Posiciones(fila,columna):
	# print(">>> %s %s"%(fila,columna))

	# filas: 0 -4 | columna: 0
	if fila==0 and columna==0:
		return (256,18)
	if fila==1 and columna==0:
		return (265,130)
	if fila==2 and columna==0:
		return (265,240)
	if fila==3 and columna==0:
		return (265,350)
	if fila==4 and columna==0:
		return (265,460)

	# filas: 0 -4 | columna: 1
	if fila==0 and columna==1:
		return (350,18)
	if fila==1 and columna==1:
		return (350,130)
	if fila==2 and columna==1:
		return (350,240)
	if fila==3 and columna==1:
		return (350,350)
	if fila==4 and columna==1:
		return (350,460)

	# filas: 0 -4 | columna: 2
	if fila==0 and columna==2:
		return (425,18)
	if fila==1 and columna==2:
		return (425,130)
	if fila==2 and columna==2:
		return (425,240)
	if fila==3 and columna==2:
		return (425,350)
	if fila==4 and columna==2:
		return (425,460)

	# filas: 0 -4 | columna: 3
	if fila==0 and columna==3:
		return (515,18)
	if fila==1 and columna==3:
		return (515,130)
	if fila==2 and columna==3:
		return (515,240)
	if fila==3 and columna==3:
		return (515,350)
	if fila==4 and columna==3:
		return (515,460)

	# filas: 0 -4 | columna: 4
	if fila==0 and columna==4:
		return (598,18)
	if fila==1 and columna==4:
		return (598,130)
	if fila==2 and columna==4:
		return (598,240)
	if fila==3 and columna==4:
		return (598,350)
	if fila==4 and columna==4:
		return (598,460)	

	# filas: 0 -4 | columna: 5
	if fila==0 and columna==5:
		return (670,18)
	if fila==1 and columna==5:
		return (670,130)
	if fila==2 and columna==5:
		return (670,240)
	if fila==3 and columna==5:
		return (670,350)
	if fila==4 and columna==5:
		return (670,460)


	# filas: 0 -4 | columna: 6
	if fila==0 and columna==6:
		return (740,18)
	if fila==1 and columna==6:
		return (740,130)
	if fila==2 and columna==6:
		return (740,240)
	if fila==3 and columna==6:
		return (740,350)
	if fila==4 and columna==6:
		return (740,460)

	# filas: 0 -4 | columna: 7
	if fila==0 and columna==7:
		return (815,18)
	if fila==1 and columna==7:
		return (815,130)
	if fila==2 and columna==7:
		return (815,240)
	if fila==3 and columna==7:
		return (815,350)
	if fila==4 and columna==7:
		return (815,460)

	# filas: 0 -4 | columna: 8
	if fila==0 and columna==8:
		return (900,18)
	if fila==1 and columna==8:
		return (900,130)
	if fila==2 and columna==8:
		return (900,240)
	if fila==3 and columna==8:
		return (900,350)
	if fila==4 and columna==8:
		return (900,460)

#--------------------------------------------
# 				FIN DEL CODIGO
