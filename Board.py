# TABLERO DE JUEGO / BOARD | Plants vs Zombies DUEL 4.0
#------------------------------------------------------
from Personajes import *

# Objeto: Board
class Board:

	# Atributos
	
	matrix = []
	listo = False
	turno = 1
	turno_terminado = False
	colocado = False
	jugando = False
	ganador = False

	distancia_servidor = 0
	distancia_cliente = 0

	cliente_ganador = False
	servidor_ganador = False
	cliente_perdedor = False
	servidor_perdedor = False
	empate = False

	oponent_data = None
	my_data = ""

	# Constructor
	def __init__(self):

		self.setup_matrix()

	#----------------------------------------------------
	# METODOS
	
	#----------------------------------------------------
	# FUNCION SET_PERSONAJE | Almacena las instancias de los personajes en la matriz
	
	def set_personaje(self,codigo,fila,columna,jugador="Server",soles = -100, flag = True):
		#Soles -100 es el "código" de creación por parte de syncMatrix

		# Almacena la instancia en la posición
		
		x_personaje = Personaje(codigo,jugador)

		fila = int(fila)
		columna = int(columna)

		# Verifica el espacio según el jugador
		
		#----------------------------------------------------
		#						SERVIDOR
		if jugador == "Server":

			if soles >= x_personaje.precio or soles == -100:

				if fila in range(0,5) and columna in range(0,3):
					if self.matrix[fila][columna] == None:

						self.matrix[fila][columna] = x_personaje
						self.colocado = True
						print(">>> PERSONAJE DE SERVIDOR CREADO")

					# Casos de ERROR
					else:
						print(">>> ESPACIO OCUPADO")
						return 1
				else:
					print(">>> UBICACION NO VALIDA") 
					return 2
			else:
				print(">>>NO HAY SOLES SUFICIENTES")
				return 3

		#----------------------------------------------------
		#						CLIENTE
		elif jugador == "Client":

			print(">>> Cliente")

			if soles >= x_personaje.precio or soles == -100:

				if fila in range(0,5) and columna in range(6,9):

					if self.matrix[fila][columna] == None:

						self.matrix[fila][columna] = x_personaje 
						self.colocado = True
						print(">>> PERSONAJE DE CLIENTE CREADO")
						
					# Casos de ERROR
					else:
						print(">>> ESPACIO OCUPADO")
						return 1
				else:
					print(">>> UBICACION NO VALIDA")
					return 2
			else:
				print(">>>NO HAY SOLES SUFICIENTES") 
				return 3

		# Si es construído directamente guarda la información en my_data, si 
		# es creado por la función de sincronía, no lo hace.
		if flag and self.colocado:
			self.my_data += "{};{};{},".format(codigo,fila,columna)
			print(self.my_data)
		        
		    # Imprime la matriz
			self.debug()

    #----------------------------------------------------
	# FUNCION TERMINAR_TURNO | Indica que el turno ha concluído
	
	def terminar_turno(self):

		self.listo = True
		# self.turno_terminado = True
		if self.oponent_data != None:
			self.sync_matrix()
			self.jugar()
			
	#-----------------------------------------------------
	# FUNCION SETUP_MATRIX | Se encarga de crear la matriz
	
	def setup_matrix(self):

		# Itera para crear la matriz
		for f in range(5):
			columna = []
			for c in range(9):
				columna.append(None)
			self.matrix.append(columna)


	#----------------------------------------------------
	# FUNCION SET_OPONENT_DATA | Se encarga de almacenar la información del 
	# oponente en una variable para luego llamar a la función de 
	# sincronización de las matrices. 
	
	def set_oponent_data(self,data):

		self.oponent_data = data
		if self.listo:
			self.sync_matrix()
			self.jugar()


	#----------------------------------------------------
	# FUNCION SYNC MATRIX | Coloca los personjes del oponente en la matriz
	
	def sync_matrix(self):

		# Se encarga primero de separar la información
		self.oponent_data = self.oponent_data.split(",")
		# Itera en cada elemento de la lista formada
		for info in self.oponent_data:
		    # Este condicional elimina el último elemento vacío generado por el 
		    # primer .split()
		    if len(info) > 1:
		        # Separa cada elemento del string cada ";" en una lista
		        # y por el orden dado, obtenemos:
		        # info[0] = código
		        # info[1] = pos x
		        # info[2] = pos y
		        # --------------------
		        info = info.split(";")
		        codigo = info[0]
		        x = info[1]
		        y = info[2]

		        # Crear los personajes
		        # ----------------------
		        
		        # Si es del cliente
		        if "2" in codigo:
		        	self.set_personaje(codigo= codigo, fila= x, columna = y,jugador = "Client",flag = False)
		        else:
			        # Si es del Servidor
			        self.set_personaje(codigo= codigo, fila= x, columna = y,flag = False)

		    else:
		        break
		self.oponent_data = None
		self.listo = False
		self.jugando = True


	#----------------------------------
	# DEBUG | Muestra la matrix de forma ordenada en la consola
	def debug(self):
		for row in range(5):
		 	print("")
		 	for col in range(9):
		 		print(self.matrix[row][col], end="")
		 		print(" ", end="")
		 	print("\n")

	#---------------------------------------------------------------------------------------
	#------------------------------						 -----------------------------------
	#------------------------------	   JUEGO EN ACCION   -----------------------------------
	#------------------------------						 -----------------------------------
	#---------------------------------------------------------------------------------------

	#-------------------------------------------------------------------------------------------
	# METODO DE JUEGO DEL SERVIDOR | Se encarga de recorrer la lista en busca de personajes y realiza movimientos a esos personajes
	def juego_servidor(self):

	    # Va recorriendo las filas
	    for fila in self.matrix:
	        
	        # Columnas 
	        index=0

	        # Mientras columna sea index de la matriz
	        while index<len(fila): 

	            # Cuando termina de recorrer la fila rompe el bucle
	            if index==len(fila)-1:
	                print('Posicion '+str(index+1),'\n','final de la lista')
	                break

	            # --------------------else----------------------------------
	            # Revisa si la posición es vacía
	            if fila[index] == None:
	                print('Posicion '+str(index+1))
	            
	            else:

	                print("El personaje %s se encuentra en la posicion %s"%(fila[index],index+1))

	                rango_ataque = 1
	                while rango_ataque <= fila[index].rango_ataque:

	                    # Es para revisar si la planta puede atacar en el rango dado, de lo contrario
	                    # continua el proceso
	                    # 
	                    if fila[index+rango_ataque] == None:
	                        print('>>> Hay un espacio vacio al frente')
	                        rango_ataque += 1
	                        break

	                    # Situación de ataque de personajes
	                    else:
	                        print("hay un personaje en el rango de "+str(rango_ataque)+'espacio(s)')
	                        print ("el personaje es "+fila[index+rango_ataque].codigo)

	                        # Revisa que sea enemigo (el nombre del jugador, con el jugador al que pertenece el objeto) y ataca
	                        if "Client" in fila[index+rango_ataque].jugador:
	                            fila[index+rango_ataque].puntos_vida -= fila[index].puntos_ataque
	                            print(">>> ATAQUE")

	                            # Revisa que el enemigo siga con vida, si no: MUERE!
	                            if fila[index+rango_ataque].puntos_vida <= 0:
	                                fila[index+rango_ataque] = None
	                                break
	                            else:
	                                break
	                        # Si un personaje propio, continua con el bucle
	                        
	                        else:
	                            print(">>> El personaje del frente es amigo")
	                            rango_ataque+=1                   
	                        # SOLO UN ATAQUE POR SECUENCIA
	                       
	            # ---------------------------------------------------
	            # Secuencia que pide movimiento
	            if fila[index] != None:

	                        movement_count=0
	                        #---------------------------------------------------------------------------
	                        while fila[index+1] == None and movement_count<fila[index].rango_mov:
	                            
	                            # Aquí está el error
	                            if "Server" in fila[index].jugador:
	                                print("SE MUEVE")
	                                tmp = fila[index]
	                                fila[index]= fila[index+1]
	                                fila[index+1]=tmp
	                                movement_count+=1
	                                index+=1
	                                # Revisa que ha llegado al final del tablero, por lo que es ganador.
	                                if index == 8:
	                                	self.ganador = True
	                                	if fila[index].jugador == "Client":
	                                		self.cliente_ganador = True
	                                		self.servidor_perdedor = True
	                                	if  fila[index].jugador == "Server":
	                                		self.servidor_ganador = True
	                                		self.cliente_perdedor = True
	                             
	                                	return 1
	                            else:
	                                break
	                        #---------------------------------------------------------------------------
	            # Aumenta el índice para el while
	            index+=1

	#-------------------------------------------------------------------------------------------
	# METODO DE JUEGO DEL CLIENTE | Se encarga de recorrer la lista en busca de personajes y realiza movimientos a esos personajes
	def juego_cliente(self):

	    for fila in self.matrix:
	        
	        # Agarra la última posición de la lista
	        index=-1

	        while abs(index) < len(fila):
	            if abs(index)-2==len(fila)-1:
	                print('Posicion '+str(index-8),'\n','final de la lista')
	                break
	            if fila[index] == None:
	                print('Posicion '+str(index+1))
	            else:
	                print(">>> El personaje %s se encuentra en la posicion %s"%(fila[index].codigo,index+1))
	                
	                rango_ataque =-1 

	                while rango_ataque <= fila[index].rango_ataque:
	                    if fila[index+rango_ataque] == None:
	                        print('>>> Hay un espacio vacio al frente')
	                        rango_ataque += -1
	                        break
	                    else:

	                        print("hay un personaje en el rango de "+str(rango_ataque)+'espacio(s)')
	                        if "Server" in fila[index+rango_ataque].jugador:
	                            fila[index+rango_ataque].puntos_vida -= fila[index].puntos_ataque
	                            if fila[index+rango_ataque].puntos_vida <= 0:
	                               	fila[index+rango_ataque] = None
	                                break
	                            else:
	                                break
	                        else:
	                            print('el personaje del frente es amigo'+'\n')
	                            rango_ataque+=1

	            if fila[index] != None:
	                        movement_count=0
	                        while fila[index-1] == None and movement_count < fila[index].rango_mov:
	                            if "Client" in fila[index].jugador:
	                                tmp = fila[index]
	                                fila[index]= fila[index-1]
	                                fila[index-1]= tmp
	                                movement_count+=1
	                                index-=1
	                                if index==-8:
	                                	self.ganador = True
	                                	if fila[index].jugador == "Client":
	                                		self.cliente_ganador = True
	                                		self.servidor_perdedor = True
	                                	if  fila[index].jugador == "Server":
	                                		self.servidor_ganador = True
	                                		self.cliente_perdedor = True
	                        
	                            else:
	                                break
	            index-=1

	
	#-------------------------------------------------------------------------------------------
	# METODO REVISAR GANADOR | Se encarga de revisar la matriz para saber cuál jugador quedó más cerca del area contrario
	
	def revisar_final(self):
		
		# itera sobre la matriz buscando los personajes
		for fila in self.matrix:
				num_fila = self.matrix.index(fila)
				for columna in fila:
					num_col = fila.index(columna)
					personaje = self.matrix[num_fila][num_col]						
					# Revisa a quién pertenece el personaje
					if personaje != None:
						if personaje.jugador == "Server" and num_col > self.distancia_servidor:
							print(num_col,self.distancia_servidor)
							self.distancia_servidor = num_col
						elif personaje.jugador == "Client" and 8-num_col < self.distancia_cliente:
								self.distancia_cliente = 8-num_col
		self.ganador = True

		if self.distancia_servidor == self.distancia_cliente:
			self.empate = True
		elif self.distancia_cliente > self.distancia_servidor:
			self.cliente_ganador = True
			self.servidor_perdedor = True
		else:
			self.empate = True


	#-------------------------------------------------------------------------------------------
	# METODO JUGAR | Se encarga de llevar los turnos, de detectar si alguno ha ganado, etc
	
	def jugar(self):

		# Cambio momentaneo a 5 turnos | El real es a los 15
		if self.turno <= 15:

			if not self.ganador:

				
				self.juego_servidor()
				self.juego_cliente()
				
				self.turno_terminado = False
				self.turno +=1
				self.jugando = False

			else:
				print("¡GANADOR!")
				self.jugando = False
		else:
			self.revisar_final()
			self.jugando = False

#--------------------------------------------------------------------------------------------
#								FIN DEL CODIGO 