# COLORES DISPONIBLES
# -------------------
# 
# En pygame los colores se dan en formato RGB

black = (0,0,0)
white = (255,255,255)
red = (255,0,0)
blue = (0,0,255)
yellow = (255, 250, 2)
green = (14, 204, 17)
ligth_blue = (13, 200, 204)
ligth_green = (68, 237, 11)
orange = (252, 155, 0)
purple = (133, 0, 242)
dark_blue = (0, 13, 91)
dark_green = (1, 66, 0)
dark_red = (114, 1, 1)