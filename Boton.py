# CLASE BOTON
# ---------------------------------
# Permite crear y utilizar botónes 

# Objeto: Botón
class Boton:

	# Atributos
	# ------------------------
	
	# Posición
	pos_x = 100
	pos_y = 100

	# Dimensiones
	alto = 50
	ancho = 100

	# Apariencia
	img1 = None # Imágen del hover
	img2 = None # Imágen base
	clicked = False # Indica si el botón es presionado

	# CONSTRUCTOR
	# --------------------------------------------------
	
	def __init__(self, pos_x, pos_y, alto, ancho, img1 = None, img2=None):

		# Posición y dimensiones
		
		self.pos_x = pos_x
		self.pos_y = pos_y

		self.alto = alto
		self.ancho = ancho

		self.img1 = img1
		self.img2 = img2

	# Métodos
	# --------------------------------------------------

	# FUNCION DRAW | Dibuja en pantalla un rectángulo de color
	
	def draw(self, display):

		if self.clicked:
			pygame.draw.rect(display, red, Rect, (self.pos_x,self.pos_y,self.ancho,self.alto))

		else:
			pygame.draw.rect(display, blue, Rect, (self.pos_x,self.pos_y,self.ancho,self.alto))					

	# FUNCION COLOCAR_BTN | Coloca en la pantalla el botón (Cuando es con imágenes)
	
	def colocar_btn(self, display):

		if self.clicked:
			display.blit(self.img1,(self.pos_x,self.pos_y,self.ancho,self.alto))
		else:
			display.blit(self.img2,(self.pos_x,self.pos_y,self.ancho,self.alto))

	# FUNCION CHECK_CLICK | Detecta si se hace click sobre algún botón
	
	def check_click(self, mouse_x, mouse_y):

		self.clicked = False

		# Se asegura que el mouse se pase dentro de las dimensiones en "Y" del botón 
		if self.pos_y <= mouse_y <= self.pos_y+self.alto:

			# Se asegura que el mouse se pase dentro de las dimensiones en "X" del botón
			if self.pos_x <= mouse_x <= self.pos_x+self.ancho:
				self.clicked = True
				
		return self.clicked

