# SPRITES DE PERSONAJES | Plants vs Zombies 4.0

# Importar los módulos
import pygame
from pygame.locals import *
from posiciones import *

# OBJETO: Sprite --> Hereda de sprite
class PersonajeSprite(pygame.sprite.Sprite):

	# Atributos
	# ------------
	
	pos_x = 0
	pos_y = 0
	rect = None
	imagen = None

	# Constructor
	# ----------------------------
	def __init__(self, imagen_url):

		# Constructor de Sprite
		pygame.sprite.Sprite.__init__(self)

		# Carga la imagen
		self.imagen = pygame.image.load(imagen_url)
		self.rect = self.imagen.get_rect()		

	# Función encarga de dibujar el sprite en la pantalla
	# ---------------------------------------------------
	def colocar_sprite(self, surface, fila, columna):

		# Define la posición que tedrá el sprite llamando a la f(x) posición
		# print(">>> fila: %s | col: %s"%(fila,columna))
		self.pos_x,self.pos_y = Posiciones(fila,columna)
		# print(self.pos_x,self.pos_y)

		# Se coloca la imagen
		surface.blit(self.imagen, (self.pos_x,self.pos_y))

#-------------------------------------------------------------------
#					FIN DEL CODIGO

