# CODIGO MAIN | PvZ DUEL 4.0
# ---------------------------

# Importar módulos
import pygame
from Ventana_servidor import *
from Ventana_menu import *
from Ventana_cliente import *


# Inicializar pygame
pygame.init()


# Ventanas

ventana_servidor = None
ventana_cliente = None
#----------------------------------------------------------
# La ventana de menú es la que primera en crearse y abrirse
ventana_menu = Ventana_menu()
ventana_menu.start()

# Variables de pygame

clock = pygame.time.Clock()
running = True # Es la que mantiene activo el main loop

#----------------------------------------------------------
#					+--------------+
#					|	MAIN LOOP  |
#					+--------------+
#----------------------------------------------------------

while running:
	
	# self.update_pantalla -> Dibuja las ventanas (Aunque no existan events)
	# NOTA: Estás deben ser aparte de la función start(), que se encarga de 
	# crear la ventana; esta debe exclusivamente dibujar TODO lo que sea 
	# necesario en la función. 
	# --------------------------------------------------------------------
	
	if ventana_menu != None:
		ventana_menu.update_pantalla()
	
	elif ventana_servidor != None:
		ventana_servidor.update_pantalla()
	
	elif ventana_cliente != None:
		ventana_cliente.update_pantalla()

	# Obtiene los eventos de pygame, necesarios para la actividad de las ventanas
	
	for evento in pygame.event.get():

		# Eventos de SALIDA
		
		if evento.type == pygame.QUIT:

			running = False

			# Si están abiertas otras ventanas
			
			if ventana_servidor != None:
				ventana_servidor.stop()
				# Función de cierre 
			
			if ventana_cliente != None:
				ventana_cliente.stop()
				# Función de cierre
				
		# Otros eventos
		
		else:
			# En esta parte se mantienen los "main loop", las funciones 
			# encargadas de recibir los eventos y realizar acciones de 
			# acuerdos a los eventos ingresados
			# ---------------------------------------------------------
			# Ventana MENU
			
			if ventana_menu != None:

				# Espera un resultado para la decisión sobre las ventanas
				
				select = ventana_menu.main_loop(evento)

				if select == 1:

					ventana_menu = None
					ventana_servidor = Ventana_servidor()
					ventana_servidor.start()

				if select == 2:

					ventana_menu = None
					ventana_cliente = Ventana_cliente()
					ventana_cliente.start()

				if select == 3:

					running = False 


			# Ventana SERVIDOR
			
			if ventana_servidor != None:

				# Mantiene siempre el main loop
				# ventana_servidor.control()
				ventana_servidor.main_loop(evento)
				# if ventana_servidor.arduino != None:
				

			# Ventana CLIENTE

			if ventana_cliente != None:

				# Mantiene siempre el main loop
				
				ventana_cliente.main_loop(evento) 

		#-----------------------------------------------
		# 			FIN DEL CICLO FOR
		#-----------------------------------------------
		

	# Actualiza la pantalla al final de cada ciclo
	
	pygame.display.update()
	clock.tick(60) # Actualiza a 60 f.p.s

	#-----------------------------------------------
	# 			FIN DEL CICLO WHILE
	#-----------------------------------------------

#--------------------------------------------------------

# Si sale del main loop es porque se ha cerrado la ventana, por lo tanto
# se cerrará pygame y la ventana.

pygame.quit()
quit()

#--------------------------------------------------------
#					FIN DEL CODIGO

