# CODIGO SERVIDOR | Plants vs Zombies DUEL 4.0
# ---------------------------

# Importar módulos

import pygame
import threading
import socket
import time
import serial
from Boton import *
from Board import *
from Personajes import *
from Colores import *
from Fuentes import *

# Objeto: Ventana_servidor

class Ventana_servidor:

	# Atributos
	# ------------------------------------
	
	# Variables de UI
	
	surface = None
	ancho = 1114
	alto = 602

	square_posx = 249
	square_posy = 76

	# Imágenes
	img_fondo = None

	img_btn_listo1 = None
	img_btn_listo2 = None
	
	img_btn_salir1 = None
	img_btn_salir2 = None

	img_sel_pos = None

	# Botones
	btn_listo = None

	btn_PS = None
	btn_Z = None
	btn_ZA = None
	btn_N = None

	btn_menu = None

	# Textos
	soles = 100

	texto_precio_PS = None
	texto_precio_N = None
	texto_precio_Z = None
	texto_precio_ZA = None

	texto_soles = None
	texto_turno = None

	texto_socket_state = None

	# Textos y variables de interacción con usuario
	#---------------------------------------------- 
	texto_sin_soles = None
	sin_soles = False

	texto_campo = None
	campo_ocupado = False

	texto_pos_invalida = None
	pos_invalida = False

	texto_ganador = None
	texto_perdedor = None
	texto_empate = None
	

	# Socket
	
	running = True
	socket_s = None
	socket_c = None

	# Control
	
	arduino = None
	ctrl_activo = True

	# Lógica del juego
	
	board = None
	columna = None
	fila = None

	# Comienzan en la fila 0, columna 0
	sel_fila = 0
	sel_columna = 0

	codigo_actual = None
	permiso = False

	#--------------------------------------------
	# Métodos
	#--------------------------------------------
	
	#------------------------------------------------------
	# FUNCION START | Se encarga de inicializar los procesos
	
	def start(self):

		self.set_UI()
		self.comenzar_socket_async()
		self.comenzar_control_async()
		self.board = Board()
		
	#------------------------------------------------------
	# FUNCION STOP | Se encarga de cerrar los procesos
		
	def stop(self):
	    self.running = False
	    
	    if self.arduino != None:
	    	# self.arduino.write(b'9')
	    	self.arduino.close()

	    if self.socket_s != None:
	    	self.socket_c.send("salir".encode())
	    	self.socket_s.close()
	    if self.socket_c != None:
	        self.socket_c.close()
	        self.socket_c = None

	#------------------------------------------------------
	# FUNCION SET_UI | Se encarga de crear la ventana y su contenido
	
	def set_UI(self):

		# Crear la ventana
		pygame.display.set_caption("Plants vs Zombies DUEL | SERVIDOR")
		self.surface = pygame.display.set_mode((self.ancho,self.alto))

		# Reproduce el Jingle
		self.musica()

		# Cargar las imágenes
		
		# FONDO
		self.img_fondo = pygame.image.load("MULTIMEDIA/ENTORNO/Jardin_vacio1.jpg").convert_alpha()
		self.img_fondo = pygame.transform.scale(self.img_fondo, (self.ancho,self.alto))
		self.img_fondo.get_rect().centerx = self.ancho/2
		self.img_fondo.get_rect().centery = self.alto/2

		self.img_sel_pos = pygame.image.load("MULTIMEDIA/ENTORNO/selected_square.jpg").convert_alpha()
		self.img_sel_pos = pygame.transform.scale(self.img_sel_pos, (80,100))

		# BOTON LISTO
		self.img_btn_listo1 = pygame.image.load("MULTIMEDIA/ENTORNO/listo1.jpg").convert_alpha()
		self.img_btn_listo1 = pygame.transform.scale(self.img_btn_listo1, (181,30))
		self.img_btn_listo2 = pygame.image.load("MULTIMEDIA/ENTORNO/listo2.jpg").convert_alpha()
		self.img_btn_listo2 = pygame.transform.scale(self.img_btn_listo2, (181,30))

		# BOTONES
		
		self.btn_listo = Boton( pos_x= 30, pos_y = 550, alto= 30, ancho =181,img1 = self.img_btn_listo2,
									img2= self.img_btn_listo1)

		self.btn_PS = Boton(pos_x= 11, pos_y = 110, alto= 173, ancho = 100)
		self.btn_N = Boton(pos_x= 126, pos_y = 110, alto= 173, ancho = 100)
		self.btn_Z = Boton(pos_x= 11, pos_y = 300, alto= 173, ancho = 100)
		self.btn_ZA = Boton(pos_x= 126, pos_y = 300, alto= 173, ancho = 100)

		# HAY QUE PONER UNA IMAGEN A ESTE BOTON
		self.btn_menu = Boton(pos_x= 30, pos_y = 550, alto= 30, ancho =181,img1 = self.img_btn_listo2,
									img2= self.img_btn_listo1)


		# TEXTOS
		
		self.texto_socket_state = font_peq.render ("IP: %s"%socket.gethostbyname(socket.gethostname()) ,True,dark_red)
		
		# Textos de errores
		self.texto_sin_soles = font_med.render("NO TIENES SOLES",True,dark_blue)
		self.texto_pos_invalida = font_med.render("¡CAMPO INCORRECTO!", True, dark_blue)
		self.texto_campo = font_med.render("¡CAMPO OCUPADO!", True, dark_blue)

		# Textos de GANADOR/PERDEDOR/EMPATE
		
		self.texto_ganador = font_gde.render("¡GANASTE!",True,blue)
		self.texto_perdedor = font_gde.render("¡PERDISTE!",True,blue)
		self.texto_empate = font_gde.render("¡EMPATE!",True,blue)

		self.texto_turno = font_med.render("TURNO ",True, dark_blue)

		self.texto_precio_PS = font_med.render("25",True,white)
		self.texto_precio_N = font_med.render("55",True,white)
		self.texto_precio_Z = font_med.render("25",True,white)
		self.texto_precio_ZA = font_med.render("55",True,white)


	#---------------------------------------------------------------------------------------
	#------------------------------						 -----------------------------------
	#------------------------------	   INTERFAZ UPDATE   -----------------------------------
	#------------------------------						 -----------------------------------
	#---------------------------------------------------------------------------------------
	

	#--------------------------------------------------------------------------------------------
	# FUNCION MUSICA | Se encarga de reproducir la música en loop mientras la ventana
	# se encuentra abierta.
	
	def musica(self):
		
		pygame.mixer.music.load("MULTIMEDIA/Sounds/Plants vs Zombies Soundtrack[Day Stage].mp3")
		pygame.mixer.music.set_volume(0.5)
		pygame.mixer.music.play(loops=-1, start=0.0) 
	

	#--------------------------------------------------------------------------------------------
	# FUNCION UPDATE_PANTALLA | Mantiene el contenido actualizado en cada ciclo. 
	
	def update_pantalla(self):

		# PRIMERO se dibuja el fondo
		self.surface.blit(self.img_fondo,self.img_fondo.get_rect())

		# DESPUES se dibuja lo demás
		self.btn_listo.colocar_btn(self.surface)


		# IMAGENES
		
		# Función que dibuja el selector de casilla
		self.surface.blit(self.img_sel_pos,(self.square_posx,self.square_posy))

		# Función que recorre la matriz buscando las imágenes
		
		for fila in self.board.matrix:
				num_fila = self.board.matrix.index(fila)
				for columna in fila:
					num_col = fila.index(columna)
					if columna != None:
						columna.sprite.colocar_sprite(self.surface,num_fila,num_col)

		# TEXTOS
	
		# TEXTOS SOCKETS
		
		if self.socket_c == None:
			self.surface.blit(self.texto_socket_state,(990,10))	
		else:
			self.texto_socket_state = font_peq.render("CONECTADO" ,True, red)
			self.surface.blit(self.texto_socket_state,(1000,10))

		self.texto_soles = font_med.render("%s"%self.soles,True,red)
		self.surface.blit(self.texto_soles,(121,35))

		# TEXTOS FIN DE JUEGO
		
		if self.board.ganador:
			if self.board.servidor_ganador:
				self.surface.blit(self.texto_ganador,(40,75))
			elif self.board.servidor_perdedor:
				self.surface.blit(self.texto_perdedor,(40,75))
			elif self.board.empate:
				self.surface.blit(self.texto_empate,(40,75))


		# Crear una funcion de textos temporales
		if self.sin_soles:
			t = threading.Thread(target=self.sin_soles_text)
			t.start()

		if self.campo_ocupado:
			# Funcion del texto del campo
			h = threading.Thread(target=self.ocupado_text)
			h.start()
		
		if self.pos_invalida:
			# Funcion del texto de posicion
			m = threading.Thread(target=self.incorrecto_text)
			m.start()

		self.texto_turno = font_peq.render("TURNO %s"%self.board.turno,True,dark_blue)
		self.surface.blit(self.texto_turno,(1020,559))

		self.surface.blit(self.texto_precio_PS,(55,240))
		self.surface.blit(self.texto_precio_N,(173,240))
		self.surface.blit(self.texto_precio_Z,(55,423))
		self.surface.blit(self.texto_precio_ZA,(173,423))

					
					
	#---------------------------------------------------------------------------------------
	#----------------------------						 -----------------------------------
	#----------------------------    TEXTOS TEMPORALES   -----------------------------------
	#----------------------------						 -----------------------------------
	#---------------------------------------------------------------------------------------

	# Funciones que activan el texto solo por unos segundos
	# -----------------------------------------------------
	
	def sin_soles_text(self):
		self.surface.blit(self.texto_sin_soles,(20,75))
		time.sleep(3)
		self.sin_soles = False

	def ocupado_text(self):
		self.surface.blit(self.texto_campo,(20,75))
		time.sleep(3)
		self.campo_ocupado = False

	def incorrecto_text(self):
		self.surface.blit(self.texto_pos_invalida,(20,75))
		time.sleep(3)
		self.pos_invalida = False



	#---------------------------------------------------------------------------------------
	#----------------------------						 -----------------------------------
	#----------------------------  CAPTURADOR DE EVENTOS -----------------------------------
	#----------------------------						 -----------------------------------
	#---------------------------------------------------------------------------------------

	#--------------------------------------------------------------------------------------------
	# FUNCION MAIN_LOOP | Se encarga de recibir los eventos y realizar trabajos con ellos
	
	def main_loop(self, evento):	

		# Obtiene los datos de la posición del mouse
		
		mouse_x = pygame.mouse.get_pos()[0]
		mouse_y = pygame.mouse.get_pos()[1]

		# print(pygame.mouse.get_pos())

		# Llama a las funciones de detección
		
		if evento.type == pygame.MOUSEMOTION:

			self.btn_listo.check_click(mouse_x, mouse_y)

		# Método que detecta el click sobre la pantalla
		# ---------------------------------------------
		if evento.type == pygame.MOUSEBUTTONDOWN:

			# Si se clickeo sobre un botón, actuará
			if self.btn_listo.check_click(mouse_x, mouse_y) and self.board.my_data != "" and not self.board.turno_terminado and self.socket_c != None and not self.board.listo:
				print("Click en el boton LISTO")
				print(self.board.my_data)
				self.socket_c.send(self.board.my_data.encode())
				if self.arduino != None:
					print(">>> ENTRA")
					self.arduino.write(b'8')
					print(">>> LO RECIBE")
				self.board.my_data = ""
				self.board.listo = False
				self.board.terminar_turno()
				self.soles = 100
				

			elif self.btn_PS.check_click(mouse_x,mouse_y) and not self.board.turno_terminado:
				print(">>> PEASHOOTER")
				self.codigo_actual = "PS01"
				self.permiso = True
				print(self.codigo_actual)
				
			elif self.btn_N.check_click(mouse_x,mouse_y) and not self.board.turno_terminado:
				print(">>> NUT")
				self.codigo_actual = "N001"
				self.permiso = True
				print(self.codigo_actual)

			elif self.btn_Z.check_click(mouse_x,mouse_y) and not self.board.turno_terminado:
				print(">>> ZOMBIE")
				self.codigo_actual = "Z001"
				self.permiso = True 
				print(self.codigo_actual)
				
			elif self.btn_ZA.check_click(mouse_x,mouse_y) and not self.board.turno_terminado:
				print(">>> ARMORED ZOMBIE")
				self.codigo_actual = "ZA01"
				self.permiso = True
				print(self.codigo_actual)
			
				
			# Si se hace click sobre la Matriz
			# ------------------------------------------
			
			# Calculo para pasar de pixeles a posiciones de la lista desde (0,0) a (4,8)
			self.columna = ((mouse_x - 255)//80)
			self.fila = ((mouse_y - 76)//100)

			print(self.fila,self.columna)
		
		# --------------------------------------------------------------------------------
		# --------------------------------------------------------------------------------
		#  Control mediante el teclado
		#  
		if evento.type == pygame.KEYDOWN:
			
			keys = pygame.key.get_pressed()
			
			if keys[K_RIGHT]:
				if  249 <= self.square_posx <= 330:
					self.square_posx += 80
					if self.sel_columna in range(0,3):
						self.sel_columna += 1
			
			if keys[K_LEFT]:
				if  329 <= self.square_posx <= 409:
					self.square_posx -= 80
					if self.sel_columna in range(0,3):
						self.sel_columna -= 1
			
			if keys[K_DOWN]:
				if  76 <= self.square_posy <= 400:
					self.square_posy += 100
					if self.sel_fila in range(0,5):
						self.sel_fila += 1
			
			if keys[K_UP]:
				if  176 <= self.square_posy <= 488:
					self.square_posy -= 100
					if self.sel_fila in range(0,5):
						self.sel_fila -= 1

			if keys[K_RETURN]:
				print(">>> clk")
				if self.codigo_actual != None:
					self.permiso = True
					print(self.codigo_actual)
					self.colocar_personaje(fila = self.sel_fila,columna = self.sel_columna,flag  = True)

			print("s_f: %s,s_c: %s"%(self.sel_fila,self.sel_columna))

		# --------------------------------------------------------------------------------
		# --------------------------------------------------------------------------------
			
			
		self.colocar_personaje()

	#  LOOP que recibe la información del control
	
	def control(self):

			ctrl_info = self.arduino.readline().decode()
			print(ctrl_info)
			

			# Se encarga de comparar la entrada de información
			
			if ctrl_info == '0\r\n':
		 		# print("ZOMBIE")
		 		self.codigo_actual = "Z001"
		 		self.permiso = True
		 		self.colocar_personaje(fila = self.sel_fila,columna = self.sel_columna,flag  = True)


			elif ctrl_info == '1\r\n':
		 		# print("ZOMBIE A")
		 		self.codigo_actual = "ZA01"
		 		self.permiso = True
		 		self.colocar_personaje(fila = self.sel_fila,columna = self.sel_columna,flag  = True)

			elif ctrl_info == '2\r\n':
		 		# print("PEASHOOTER")
		 		self.codigo_actual = "PS01"
		 		self.permiso = True
		 		self.colocar_personaje(fila = self.sel_fila,columna = self.sel_columna,flag  = True)

			elif ctrl_info == '3\r\n':
		 		# print("NUT")
		 		self.codigo_actual = "N001"
		 		self.permiso = True
		 		self.colocar_personaje(fila = self.sel_fila,columna = self.sel_columna,flag  = True)

			elif ctrl_info == '4\r\n':
		 		# print("LISTO")
		 		if self.board.my_data != ""and not self.board.turno_terminado and self.socket_c != None and not self.board.listo:
			 		print(self.board.my_data)
			 		self.socket_c.send(self.board.my_data.encode())
			 		if self.arduino != None:
			 			print(">>> ENTRA")
			 			self.arduino.write(b'8')
			 			print(">>> LO RECIBE")
			 		self.board.my_data = ""
			 		self.board.listo = False
			 		self.board.terminar_turno()
			 		self.soles = 100

			elif ctrl_info == 'U\r\n':
		 		# print("UP")
		 		if  176 <= self.square_posy <= 488:
		 			self.square_posy -= 100
		 			if self.sel_fila in range(0,5):
		 				self.sel_fila -= 1

			elif ctrl_info == 'D\r\n':
		 		# print("DOWN")
		 		if  76 <= self.square_posy <= 400:
		 			self.square_posy += 100
		 			if self.sel_fila in range(0,5):
		 				self.sel_fila += 1

			elif ctrl_info == 'L\r\n':
		 		# print("LEFT")
		 		if  329 <= self.square_posx <= 409:
		 			self.square_posx -= 80
		 			if self.sel_columna in range(0,3):
		 				self.sel_columna -= 1

			elif ctrl_info == 'R\r\n':
		 		# print("RIGHT")
		 		if  249 <= self.square_posx <= 330:
		 			self.square_posx += 80
		 			if self.sel_columna in range(0,3):
		 				self.sel_columna += 1



	#-------------------------------------------------------------------------------------
	# FUNCION COLOCAR PERSONAJE | Se encarga de crear instancias de personaje en la matriz		
			
	def colocar_personaje(self, fila = None,columna = None, flag = False):

		# print(">>> ", self.codigo_actual)
		
		# Valida que se haya permiso
		if self.permiso:

			if self.soles >= 0:
				# Valida que el click se haga en una posición válida
				
				if self.fila in range(0,5) and self.columna in range(0,9) or fila in range(0,5) and columna in range(0,3) and self.permiso:

					if self.columna in range(0,6) or columna in range(0,3):
						if flag:
							print("COLOCANDO EN %s,%s"%(fila,columna))
							accion = self.board.set_personaje(self.codigo_actual,fila,columna,
							"Server",self.soles)
						else:
							print("COLOCANDO EN %s,%s"%(self.fila,self.columna))
							accion = self.board.set_personaje(self.codigo_actual,self.fila,self.columna,
								"Server",self.soles)

						if accion == 1:
							self.campo_ocupado = True

						elif accion == 2:
							self.pos_invalida = True

						elif accion == 3:
							self.sin_soles = True

						# Si el personaje se colocó, se restan los soles
						if self.board.colocado:
							if flag:
								self.soles -= self.board.matrix[fila][columna].precio
							else:
								self.soles -= self.board.matrix[self.fila][self.columna].precio
							if self.soles <= 0:
								self.soles = 0
							self.board.colocado = False
							pygame.display.update()
						self.permiso = False 
						self.codigo_actual = None

			# Si se hace click en una posición no permitida | Se debe clickear otra posición 
			# y enviará una posición invalida ***
			

	#---------------------------------------------------------------------------------------
	#------------------------------						 -----------------------------------
	#------------------------------	COMUNICACION SOCKETS -----------------------------------
	#------------------------------						 -----------------------------------
	#---------------------------------------------------------------------------------------
	
	#--------------------------------------------------------------------------------------------
	# FUNCION COMENZAR_SOCKET_ASYNC | Crea un HILO para inciar la comunicación mediante sockets.
	
	def comenzar_socket_async(self):
		
		hilo = threading.Thread(target = self.comenzar_socket)
		hilo.start()

	#--------------------------------------------------------------------------------------------
	# FUNCION COMENZAR_SOCKET | Se encarga de la conexión y comunicación con el cliente
	
	def comenzar_socket(self):

		# Dirección IP y puerto
		host = socket.gethostbyname(socket.gethostname())
		port = 8000

		# Socket del servidor
		self.socket_s = socket.socket()
		self.socket_s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR, 1)
		self.socket_s.bind((host,port))
		self.socket_s.listen(10)

		# Conexión
		print(">>> ESPERANDO CLIENTE, %s"%host)
		self.socket_c,(host_c,port_c) = self.socket_s.accept() # Acepta al cliente y obtiene una tupla (nombre, dirección)
		self.socket_c.setblocking(False)

		print(">>> CLIENTE CONECTADO")

		# Loop de escucha
		while self.running:

			# print(">>> ESCUCHANDO DE CLIENTE")
			try:

				# El socket lee a 1024 bytes y lo decodifica a uns str
				data = self.socket_c.recv(1024).decode()

				# En caso de que el cliente se cierre envía un mensaje salir para cerrar la conexión.
				if data == "salir":
					self.running = False
					print(">>> CONECCION TERMINADA")
					self.stop()
				elif len(data) > 1:
					print(">>> RECIBIDO: %s", data)
					self.board.set_oponent_data(data)
					if self.arduino != None:
						print(">>> ENTRA")
						self.arduino.write(b'9')
						print(">>> LO RECIBE")
			
			# Le debe enviar la información a Board para que actualice la matrix. 
		
			except:

				# Si no consigue recibir inf@ ha ocurrido un error 
				# print(">>> OOPS! HA OCURRIDO UN ERROR")
				pass

		
	#---------------------------------------------------------------------------------------
	#------------------------------						 -----------------------------------
	#------------------------------	 ARCADE GAME CONTROL -----------------------------------
	#------------------------------						 -----------------------------------
	#---------------------------------------------------------------------------------------
	
	#--------------------------------------------------------------------------------------------
	# FUNCION COMENZAR_CONTROL_ASYNC | Crea un HILO para inciar la comunicación serial con el arduino.
	
	def comenzar_control_async(self):
		hilo = threading.Thread(target = self.comenzar_control)
		hilo.start()

	#--------------------------------------------------------------------------------------------
	# FUNCION COMENZAR_CONTROL | Se encarga de la conexión y comunicación con el control	
	
	def comenzar_control(self):

		# Puerto y baudios
		
		port = "COM3"
		baudios = 9600

		# Control
		
		while True:
			if self.arduino == None:
				# INTENTA conectarse con el arduino cada vez
				try:
					
					self.arduino = serial.Serial(port,baudios)
					print(">>> CONTROL CONECTADO")
					
				except:
					pass
			else:
				self.control()

#--------------------------------------------------------------------------------------------
#								FIN DEL CODIGO 